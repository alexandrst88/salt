
/var/www/redmine:
  file.directory:
    - user: www-data
    - group: www-data
    - file_mode: 744
    - dir_mode: 755
    - makedirs: True
    - recurse:
      - user
      - group
      - mode

www-symlink:
  file.symlink:
    - name: /var/www/redmine
    - target: /usr/share/redmine/public
    - force: True

/usr/share/redmine/config/database.yml:
 file.managed:
    - source: salt://database.yml
    - user: root
    - group: root
    - mode: 644


first_migration:
    cmd.run:
        - name: bundle install
        - cwd: /usr/share/redmine

migration:
 cmd.run:
        - user: root
        - name: RAILS_ENV=production rake db:migrate
        - cwd: /usr/share/redmine/

/usr/share/redmine/public/plugin_assets:
  file.directory:
    - user: www-data
    - group: www-data
    - file_mode: 744
    - dir_mode: 755
    - makedirs: True
    - recurse:
      - user
      - group
      - mode


add_key:
 cmd.run:
        - user: root
        - name: apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 561F9B9CAC40B2F7
        - cwd: /usr/share/redmine/

certificates:
   pkg:
    - installed
    - pkgs:
        - apt-transport-https
        - ca-certificates

apt-update:
 cmd.run:
        - user: root
        - name: apt-get update

install_redmine:
  cmd.run:
        - user: root
        - name: bundle install --without development test postgresql sqlite
        - cwd: /usr/share/redmine/

secret_token:
  cmd.run:
        - user: root
        - name: sudo rake generate_secret_token
        - cwd: /usr/share/redmine
