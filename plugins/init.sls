unzip:
  pkg.installed:
     - pkg:
        -name: unzip

agile-plugin:
  archive.extracted:
    - name: /usr/share/redmine/plugins/
    - source: http://redminecrm.com/license_manager/15337/redmine_agile-1_3_5-light.zip
    - archive_format: zip
    - source_hash: md5=e4c4aefbc9e16f3a82257baea3bf56f3
    - if_missing: /opt/redmine_agile-1_3_5-light.zip
  cmd.run:
    - name: mv /usr/share/redmine/plugins/redmine_agile-1_3_5-light.zip /usr/share/redmine/plugins/redmine_agile

agile_install:
    cmd.run:
        - user: root
        - name: bundle install
        - cwd: /usr/share/redmine/plugins/redmine_agile
agile_migration:
    cmd.run:
        - user: root
        - name: rake redmine:plugins NAME=redmine_agile RAILS_ENV=production
        - cwd: /usr/share/redmine/plugins/redmine_agile

crm-plugin:
  archive.extracted:
    - name: /usr/share/redmine/plugins/
    - source: http://redminecrm.com/license_manager/15889/redmine_contacts-3_4_4-light.zip
    - archive_format: zip
    - source_hash: md5=a58bbbbe88f4d482429183b64ca84cc4
    - if_missing: /opt/redmine_contacts-3_4_4-light.zip
  cmd.run:
    - name: mv /usr/share/redmine/plugins/redmine_contacts-3_4_4-light.zip /usr/share/redmine/plugins/redmine_contacts

crm_install:
    cmd.run:
        - user: root
        - name: bundle install
        - cwd: /usr/share/redmine/plugins/redmine_contacts
crm_migration:
    cmd.run:
        - user: root
        - name: rake redmine:plugins RAILS_ENV=production
        - cwd: /usr/share/redmine/plugins/redmine_agile
google_oath:
    cmd.run:
        - user: root
        - name: git clone https://github.com/twinslash/redmine_omniauth_google.git
        - cwd: /usr/share/redmine/plugins/
google_oath_install:
    cmd.run:
        - user: root
        - name: bundle install
        - cwd:  /usr/share/redmine/plugins/redmine_omniauth_google
google_migration:
    cmd.run:
        - user: root
        - name: rake redmine:plugins RAILS_ENV=production
        - cwd: /usr/share/redmine/plugins/redmine_omniauth_google

time_tracker:
    cmd.run:
        - user: root
        - name: git clone https://github.com/fernandokosh/redmine_time_tracker.git
        - cwd: /usr/share/redmine/plugins/
time_tracker_install:
    cmd.run:
        - user: root
        - name: bundle install
        - cwd:  /usr/share/redmine/plugins/redmine_time_tracker
time_tracker_migration:
    cmd.run:
        - user: root
        - name: rake redmine:plugins RAILS_ENV=production
        - cwd: /usr/share/redmine/plugins/redmine_time_tracker
