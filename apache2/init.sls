/etc/apt/sources.list.d/passenger.list:
 file.managed:
    - source: salt://apache2/files/passenger.list
    - user: root
    - group: root
    - mode: 600

passenger:
  pkg.installed:
    - pkg:
      - name: libapache2-mod-passenger
Enable rewrite module:
    apache_module.enable:
        - name: rewrite

Enable ssl module:
    apache_module.enable:
        - name: ssl
Enable headers module:
    apache_module.enable:
        - name: headers

/etc/apache2/ssl:
  file.directory:
    - user: www-data
    - group: www-data
    - file_mode: 600
    - dir_mode: 600
    - makedirs: True
    - recurse:
      - user
      - group
      - mode

/etc/apache2/ssl/redmine.crt:
  file.managed:
    - source: salt://apache2/files/redmine.crt
    - mode: 755
/etc/apache2/ssl/redmine.key:
  file.managed:
    - source: salt://apache2/files/redmine.key
    - mode: 755

/etc/apache2/sites-available/redmine.conf:
  file.managed:
    - source: salt://apache2/files/redmine-vhost
    - mode: 755
site-enable:
  cmd.run:
        - name: a2ensite redmine.conf
site-disable:
  cmd.run:
        - name: a2dissite 000-default.conf
stuff:
  cmd.run:
      - name: apt-get install libapache2-mod-passenger -y
apache-restart:
  cmd.run:
        - name: service apache2 restart
