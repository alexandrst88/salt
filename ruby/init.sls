update-pkgs:
    cmd.run:
        - name: apt-get update
redmine_apache:
  pkg:
    - installed
    - pkgs:
        - build-essential
        - python-mysqldb
redmine_stuff:
  pkg:
    - installed
    - pkgs:
      - ruby1.9.3
      - ruby1.9.1-dev
      - ri1.9.1
      - libruby1.9.1
      - libssl-dev
      - zlib1g-dev
      - apache2
      - libapache2-mod-php5
      - libapache2-mod-perl2
      - libcurl4-openssl-dev
      - libssl-dev
      - apache2-prefork-dev
      - libapr1-dev
      - libaprutil1-dev
      - libmysqlclient-dev
      - libmagickcore-dev
      - libmagickwand-dev
      - curl
      - git-core
      - patch
      - build-essential
      - bison
      - zlib1g-dev
      - libssl-dev
      - libxml2-dev
      - sqlite3
      - libsqlite3-dev
      - autotools-dev
      - libxslt1-dev
      - libyaml-0-2
      - autoconf
      - automake
      - libreadline6-dev
      - libyaml-dev
      - libtool
      - imagemagick
      - apache2-utils
gems:
  gem.installed:
    - name: bundler
update-gems:
    cmd.run:
        - name: gem update
untar-redmine:
  archive.extracted:
    - name: /usr/share/
    - source: http://www.redmine.org/releases/redmine-2.6.1.tar.gz
    - archive_format: tar
    - source_hash: md5=dce687396b393ccaf26fdcd3465b7436
    - if_missing: /usr/share/redmine-2.6.1
  cmd.run:
    - name: mv /usr/share/redmine-2.6.1 /usr/share/redmine
passenger-gem:
  gem.installed:
    - user: root
    - name: passenger
