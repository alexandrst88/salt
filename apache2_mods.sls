Enable rewrite module:
    apache_module.enable:
        - name: rewrite

Enable ssl module:
    apache_module.enable:
        - name: ssl
Enable headers module:
    apache_module.enable:
        - name: headers
/etc/apache2/ssl/redmine.crt:
  file.managed:
    - source: salt://redmine.crt
    - mode: 755
/etc/apache2/ssl/redmine.key:
  file.managed:
    - source: salt://redmine.key
    - mode: 755
