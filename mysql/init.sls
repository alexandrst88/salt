mysql:
  pkg.installed:
     - pkgs:
         - {{ pillar['mysql']['server'] }}
         - {{ pillar['mysql']['client'] }}
mysql_dbuser:
  mysql_user.present:
    - name: {{ pillar['mysql']['user'] }}
    - password: {{ pillar['mysql']['password'] }}

redmine_dbuser:
  mysql_user.present:
    - name: redmine
    - host: localhost
    - password: alexstep

redmine_db:
  mysql_database.present:
    - name: redmine
    - require:
      - pkg: mysql
redmine_grant:
  mysql_grants.present:
    - grant: ALL
    - database: redmine.*
    - user: redmine
    - require:
      - mysql_user: redmine_dbuser
